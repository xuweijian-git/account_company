import Vue from 'vue'
import App from './App'

Vue.config.productionTip = false

import uView from "uview-ui";
Vue.use(uView);

import commonFun from "./common/common.js"
Vue.prototype.$commonFun = commonFun;

/* 格式化时间 */
import formatTime from './common/formatTime.js';
Vue.prototype.$formatTime = formatTime;

/* 弹窗模态框组件 */
import G_show_modal from '@/components/G_show_modal/g_show_modal.js';
Vue.use(G_show_modal)

App.mpType = 'app'

const app = new Vue({
	...App
})
app.$mount()
