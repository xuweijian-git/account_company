import Vue from 'vue'
import Vuex from 'vuex'
Vue.use(Vuex)
var im = null;
const store = new Vuex.Store({
	state: {
		loginData: {},
		isLogin: false
	},
	mutations: {
		//登录
		loginDataFun(state, data) {
			state.isLogin = true;
			//直接拿着token去登录，不用密码
			state.loginData = data;
			uni.setStorageSync('userInfo', data)
			let RongIMLib = getApp().globalData.IMService;
			//初始化融云IM
			im = RongIMLib.init({
				appkey: '8luwapkv84g8l'
			})
			var user = {
				token: state.loginData.token
			};
			//传入当前用户的token连接融云
			im.connect(user)
				.then(function(user) {
					console.log('链接成功, 链接用户 id 为: ', user.id);
				})
				.catch(function(error) {
					console.log('链接失败: ', error.code, error.msg);
				});
		},
		//注销
		logoutDataFun(state) {
			//退出登录或关闭APP时断开连接
			im.disconnect().then(function() {
				console.log('断开链接成功');
			});
			state.isLogin = false;
			state.loginData = {};
			uni.removeStorageSync('userInfo')
		}
	}
})

export default store
