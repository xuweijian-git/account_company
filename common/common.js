const commonFun = {
	/* 封装原生toast轻量提示框 */
	msg: (title, duration = 1500, mask = false, icon = 'none') => {
		if (Boolean(title) === false) {
			return;
		}
		uni.showToast({
			title,
			duration,
			mask,
			icon,
			position: 'bottom'
		});
	},
	/* 封装获取节点元素尺寸 */
	getElSize: (classname) => { //元素的class名称
		return new Promise((res, rej) => {
			uni.createSelectorQuery().select('.' + classname).fields({
				size: true,
				scrollOffset: true
			}, (data) => {
				res(data);
			}).exec();
		});
	},
	deviceInfo: () => {
		return new Promise((resolve, reject) => {
			uni.getSystemInfo({
				success(res) {
					resolve(res)
				}
			})
		})
	}
}
export default commonFun
