//高德key，请不要使用本人的这个key用于生产环境
const key = 'f88a21d5a109e4ce7aa75c875513394c';

//请注意使用我在static目录下的图片资源，你也可以自己修改

const amapFile = require('./amap-uni.js'); 


function PlanningRoute(start, end, result, fail) {
	let that = this;
	var myAmapFun = new amapFile.AMapWX({
		key: key
	});

	myAmapFun.getDrivingRoute({
		origin: start,
		destination: end,
		success: function(data) {
			var points = [];
			if (data.paths && data.paths[0] && data.paths[0].steps) {
				var steps = data.paths[0].steps;
				for (var i = 0; i < steps.length; i++) {
					var poLen = steps[i].polyline.split(';');
					for (var j = 0; j < poLen.length; j++) {
						points.push({
							longitude: parseFloat(poLen[j].split(',')[0]),
							latitude: parseFloat(poLen[j].split(',')[1])
						})
					}
				}
			}
			//这个返回结果就是对应的路线坐标,其他属性页面自己配置,请参照uniapp地图组件一章节
			var str1 = start.split(",");
			var startObj1 = {
				longitude: str1[0],
				latitude: str1[1]
			};
			var str2 = end.split(",");
			var startObj2 = {
				longitude: str2[0],
				latitude: str2[1]
			};
			points.unshift(startObj1)
			points.push(startObj2)
			result({
				points: points
			})
		},
		fail: function(info) {
			fail(info)
		}
	})
}

module.exports = {
	line: PlanningRoute
}
